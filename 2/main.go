package main

import (
	"log"
	"sync"
)

func main() {
	//Переменные
	var (
		wt sync.WaitGroup
	)
	input := []int{2, 3, 4, 5, 6}

	//Создаем и запускаем несколько горутин с передачей WaitGroup и значения из массива(Либо можно реализовать через канал)
	for _, val := range input {
		wt.Add(1)
		go func(val int, wt *sync.WaitGroup) {
			log.Println(val * val)
			wt.Done()
		}(val, &wt)
	}

	//Ожидание завершения работ горутин
	wt.Wait()
}
