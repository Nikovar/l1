package main

import (
	"log"
	"strings"
)

func main() {
	inputString := "dog cat mad yep"
	sep := " "
	output := ""

	//Разделяем строку на массив строк и обратно конкатенируем в обратную сторону
	slice := strings.Split(inputString, sep)
	for i := len(slice) - 1; i >= 0; i-- {
		output += slice[i] + " "
	}
	log.Println(output)
}
