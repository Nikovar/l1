package main

import (
	"log"
)

type ICreature interface {
	yawn()
}

type Adapter struct {
	creature Human
}

func (adapter Adapter) yawn() {
	adapter.creature.yawn()
}

type Human struct {
	age int
}

func (creature Human) yawn() {
	log.Println("Is yawning")
	log.Println(creature.age)
}

func main() {

	var creature ICreature = Adapter{}

	creature.yawn()

}
