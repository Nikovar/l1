package main

import (
	"log"
)

func main() {
	inputArr := []int{1, 2, 3, 4, 97, 7, 6, 5}
	log.Println(removeFromSlice(inputArr, 3))
}

//Удаление определенного по счету элемента, начиная с 1
func removeFromSlice(input []int, order int) []int {
	result := input[:order-1]
	for _, val := range input[order:] {
		result = append(result, val)
	}
	return result
}
