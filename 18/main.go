package main

import (
	"log"
	"sync"
)

type Counter struct {
	i int
}

func main() {
	var wt sync.WaitGroup
	var counter Counter
	times := 5
	counter.i = 0

	//Запуск нескольких горутин, использующих указатель на структуру
	for i := 0; i < 5; i++ {
		wt.Add(1)
		go func(input *Counter, times int, wt *sync.WaitGroup) {
			for i := 0; i < times; i++ {
				input.i++
			}
			wt.Done()

		}(&counter, times, &wt)
	}
	wt.Wait()
	log.Println(counter.i)
}
