package main

import "log"

func main() {
	input := []string{"2", "3", "5", "7", "11", "13"}
	c := make(chan string)
	result := make(map[string]string)

	//Горутина для записи в канал
	go func(arr []string, c chan string) {
		for _, val := range input {
			c <- val
		}
	}(input, c)

	//Запуск несколькиз горутин для записи в мапу(словарь)
	for i := 0; i < 4; i++ {
		go WriteToMap(c, result)
	}
	log.Print(c)

}

func WriteToMap(c chan string, res map[string]string) {
	for i := range c {
		res[i] = "spam"
	}
}
