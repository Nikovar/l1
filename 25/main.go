package main

import (
	"log"
	"time"
)

func main() {
	log.Println("Started")
	mySleep(3)
	log.Println("Ended")

}

//Создание функции, которая ждет от канала времени, чтобы прошло нужное количество секунд
func mySleep(seconds int) {
	select {
	case <-time.After(time.Duration(seconds) * time.Second):
		return
	}
}
