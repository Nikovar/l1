package main

import "log"

func main() {
	s := "Абырвалг"
	result := ""

	//Конкатенируем строки в конец
	for _, v := range s {
		result = string(v) + result
	}
	log.Println(result)
}
