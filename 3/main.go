package main

import (
	"log"
	"sync"
)

func main() {
	var (
		wt sync.WaitGroup
	)
	input := []int{2, 3, 4, 5, 6}
	resChannel := make(chan int, len(input))

	//Запускаем несколько горутин, которые пишут в канал
	for _, val := range input {
		wt.Add(1)
		go func(val int, res chan int, wt *sync.WaitGroup) {
			resChannel <- val * val
			wt.Done()
		}(val, resChannel, &wt)
	}

	//Ожидаем завершения горутин и закрытие канала
	wt.Wait()
	close(resChannel)

	//Подсчет суммы из горутин
	result := 0
	for i := range resChannel {
		result += i
	}
	log.Print(result)

}
