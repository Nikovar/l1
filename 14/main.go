package main

import (
	"log"
	"reflect"
)

type myType struct {
}

//Используем reflect для определения типа данных
func main() {
	var x interface{} = "Hello"
	log.Println(reflect.TypeOf(x))

	var y interface{} = 1
	log.Println(reflect.TypeOf(y))

	var z interface{} = myType{}
	log.Println(reflect.TypeOf(z))

	var xBool interface{} = true
	log.Println(reflect.TypeOf(xBool))

	var xChan interface{} = make(chan int)
	log.Println(reflect.TypeOf(xChan))

}
