package main

import "log"

func main() {
	s1 := []string{"car", "cat", "dog", "cat"}
	log.Println(makeSet(s1))
}

//Создаем множество уникальных значений. На основе словаря
func makeSet(input []string) (result []string) {
	temp := make(map[string]bool)
	for _, val := range input {
		if _, ok := temp[val]; !ok {
			temp[val] = true
		}
	}
	for key := range temp {
		result = append(result, key)
	}
	return result
}
