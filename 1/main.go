package main

import (
	"log"
)

//Базовая структура(класс)
type Human struct {
	Age  int
	Name string
}

//Метод класса
func (h *Human) SetAge(age int) {
	h.Age = age
}

//Класс, наследующий базовый
type Action struct {
	Human
	Ability string
}

func (a *Action) SetAge(age int) {
	a.Age = age
}

func main() {
	human := Human{}
	human.SetAge(99)
	log.Print(human)
	action := Action{Human: human}
	log.Print(action)
	action.SetAge(1)
	log.Print(action)
	action.Human.SetAge(44)
	log.Print(action)
}
