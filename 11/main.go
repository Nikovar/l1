package main

import "log"

func main() {
	s1 := []string{"1", "2", "3"}
	s2 := []string{"hello", "one", "two", "1"}
	log.Println(intersection(s1, s2))
}

//Используем мапу для сохранения всех значений в первом множестве и поиском совпадений в другом
func intersection(s1, s2 []string) []string {
	result := []string{}
	hash := make(map[string]bool)
	for _, e := range s1 {
		hash[e] = true
	}
	for _, e := range s2 {
		if hash[e] {
			result = append(result, e)
		}
	}
	log.Println(result)
	return result
}
