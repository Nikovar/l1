Что выведет данная программа и почему?


func someAction(v []int8, b int8) {
  v[0] = 100
  v = append(v, b)
}

func main() {
  var a = []int8{1, 2, 3, 4, 5}
  someAction(a, 6)
  fmt.Println(a)
}


100,2,3,4,5. Изменения вносились ровно до того момента, как был перезаписан массив из-за append.