package main

import (
	"fmt"
	"log"
	"strconv"
	"sync"
)

func main() {
	var wt sync.WaitGroup
	ch := make(chan string)

	//Запрос на количество горутин, читающих из канала
	var numStr string
	log.Println("Enter Number of Workers")
	fmt.Scanln(&numStr)
	num, err := strconv.Atoi(numStr)
	if err != nil {
		log.Printf("Error with input number, %v", err)
	}

	//Запуск горутины для ассинхронного ввода в канал и N горутин для чтения
	log.Println("Now Enter Numbers, ctrl+c to leave")
	wt.Add(1)
	go input(ch, &wt)
	for i := 0; i < num; i++ {
		go worker(ch, i)
	}
	wt.Wait()
	defer close(ch)
}

func input(output chan string, wt *sync.WaitGroup) {
	for {
		var input string
		fmt.Scanln(&input)
		output <- input
	}
	wt.Done()
}

func worker(input chan string, i int) {
	for {
		log.Println(<-input, i)
	}
}
