package main

import (
	"strconv"
)

var justString string

//Ошибка возникает из-за возможной попытки взять больший кусок строки, чем есть в ней самой. Вне зависимости как число переводится в строку в ранее данном примере.
func someFunc() {
	v := strconv.Itoa(1 << 10)
	if len(v) < 100 {
		justString = v[:]
	} else {
		justString = v[:100]
	}
}

func main() {
	someFunc()
}
