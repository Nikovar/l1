package main

import "log"

func main() {
	var (
		x = 10
		y = 5
	)
	x = x ^ y
	y = y ^ x
	x = x ^ y
	log.Println(x)
	log.Println(y)
}

//Второй вариант swap
func swap(x, y int) {
	x, y = y, x
	log.Println(x, y)
}
