package main

func main() {
	inputStr := "AdsdsaA"
	inputStr = "ФффыВав"
	inputStr = "фывапролджэ"
	print(CheckStr(inputStr))
}

//Проверка уникальных символов
func CheckStr(input string) bool {
	temp := make(map[rune]bool)
	for _, val := range input {
		if _, ok := temp[val]; ok {
			return false
		} else {
			temp[val] = true
		}
	}
	return true
}
