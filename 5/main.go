package main

import (
	"fmt"
	"log"
	"strconv"
	"sync"
	"time"
)

func main() {
	var wt sync.WaitGroup
	ch := make(chan string)
	var numStr string

	//Запрос на количество секунд для работы
	log.Println("Enter Number of Seconds to Work")
	fmt.Scanln(&numStr)
	start := time.Now()

	log.Println("Now Enter Numbers, ctrl+c to leave")
	num, err := strconv.Atoi(numStr)
	if err != nil {
		log.Printf("Error with input number, %v", err)
	}

	//Запуск горутин - таймера, ввода и чтения, ожидание окончания работы таймера
	wt.Add(1)
	go EndTimer(start, num, &wt)
	go Input(ch)
	go Worker(ch)

	wt.Wait()
	defer close(ch)
}

//Просто закидывает значения в канал
func Input(output chan string) {
	for {
		var input string
		fmt.Scanln(&input)
		output <- input
	}
}

//Горутина, проверяющая каждый раз время на основе Unix timestamp
func EndTimer(start time.Time, seconds int, wt *sync.WaitGroup) {
	for {
		if time.Now().Unix() > start.Add(time.Second*time.Duration(seconds)).Unix() {
			wt.Done()
			return
		}
	}
}

//Просто читает значения из канала
func Worker(input chan string) {
	for {
		log.Println(<-input)
	}
}
