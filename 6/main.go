package main

import (
	"context"
	"log"
	"time"
)

func main() {
	//Запуск горутины, которая читает, пока в канал не закроется и она не сможет оттуда читать.
	ch := make(chan string, 2)
	go func() {
		for {
			v, ok := <-ch
			if !ok {
				log.Println("Finish")
				return
			}
			log.Println(v)
		}
	}()

	ch <- "hello"
	ch <- "world"
	close(ch)

	//Использование контекста для остановки горутины
	ch1 := make(chan struct{})
	ctx, cancel := context.WithCancel(context.Background())

	go func(ctx context.Context) {
		for {
			select {
			case <-ctx.Done():
				ch1 <- struct{}{}
				return
			default:
				log.Println("foo...")
			}

			time.Sleep(500 * time.Millisecond)
		}
	}(ctx)

	go func() {
		time.Sleep(3 * time.Second)
		cancel()
	}()

	<-ch1
	log.Println("Finish")
}
