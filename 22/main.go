package main

import (
	"fmt"
	"math/big"
)

func main() {
	var a, b string
	fmt.Println("Enter number a")
	fmt.Scanln(&a)
	fmt.Println("Enter number b")
	fmt.Scanln(&b)

	//Реализация через big.Int. Также можно сделать калькулятор на основе двух крайне больших строк
	aInt := new(big.Int)
	aInt.SetString(a, 10)
	bInt := new(big.Int)
	bInt.SetString(b, 10)
	res := new(big.Int)
	fmt.Println(res.Div(aInt, bInt))
	fmt.Println(res.Mul(aInt, bInt))
	fmt.Println(res.Add(aInt, bInt))
	fmt.Println(res.Sub(aInt, bInt))
}
