package main

import (
	"log"
	"sync"
)

func main() {
	var wt sync.WaitGroup
	input := []int{2, 3, 4, 5, 6}
	numbers := make(chan int, len(input))
	squares := make(chan int, len(input))

	//Запуск конвеера чисел. Чтение из массива и запись в канал
	wt.Add(1)
	go func(input []int, c chan int, wt *sync.WaitGroup) {
		for _, i := range input {
			c <- i
		}
		defer close(c)
		wt.Done()
	}(input, numbers, &wt)

	//Чтение из канала, запись в канал квадрата прочтенного значения
	wt.Add(1)
	go func(in chan int, out chan int, wt *sync.WaitGroup) {
		for i := range in {
			out <- i * i
		}
		defer close(squares)
		wt.Done()
	}(numbers, squares, &wt)
	wt.Wait()
	for i := range squares {
		log.Print(i)
	}
}
