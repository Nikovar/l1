package main

import (
	"log"
	"math"
)

type Point struct {
	x int
	y int
}

func (p *Point) New(x, y int) {
	p.x = x
	p.y = y
}

func GetDistance(p1, p2 *Point) (dist int) {
	a := float64(p1.x - p2.x)
	b := float64(p1.y - p2.y)
	dist = int(math.Sqrt(float64(math.Pow(a, 2) + math.Pow(b, 2))))
	return dist
}

func main() {
	var (
		p1 Point
		p2 Point
	)
	p1.New(1, 2)
	p2.New(3, -10)
	log.Println(GetDistance(&p1, &p2))

}
