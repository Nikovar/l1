package main

import (
	"log"
	"math/rand"
)

func main() {
	inputArr := []int{380, -312, 925, 158, -46, 177, 22, -482, 273, 217, 514, -39}
	log.Println(quicksort(inputArr))
}

func quicksort(a []int) []int {
	//Проверка на размер
	if len(a) < 2 {
		return a
	}

	left, right := 0, len(a)-1

	pivot := rand.Int() % len(a)

	a[pivot], a[right] = a[right], a[pivot]

	for i, _ := range a {
		if a[i] < a[right] {
			a[left], a[i] = a[i], a[left]
			left++
		}
	}

	a[left], a[right] = a[right], a[left]

	quicksort(a[:left])
	quicksort(a[left+1:])

	return a
}
