package main

import "fmt"

func main() {
	var a int
	var n int

	//Чтение числа
	fmt.Println("Input number")
	fmt.Scanf("%d", &a)

	//Чтение, какой бит будет изменен
	fmt.Println("Input n bit")
	fmt.Scanf("%d", &n)

	//Производим XOR операцию N-ного бит для изменения его на противоположное значение
	a = a ^ (1 << (n - 1))
	fmt.Print(a)
}
