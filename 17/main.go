package main

import (
	"log"
	"sort"
)

func main() {
	value := 22
	inputArr := []int{100, 23, 21, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	log.Println(binarySearch(inputArr, value))

}

func binarySearch(input []int, search int) bool {
	//Сортируем данные для упрощения дальнейшего поиска
	sort.SliceStable(input, func(i, j int) bool {
		return input[i] < input[j]
	})

	low := 0
	high := len(input) - 1

	for low <= high {
		//Берем центральный элемент в остающемся массиве.
		median := (low + high) / 2

		//
		if input[median] < search {
			low = median + 1
		} else {
			high = median - 1
		}
	}

	if low == len(input) || input[low] != search {
		return false
	}

	return true
}
