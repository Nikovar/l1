package main

import (
	"log"
	"math"
)

func main() {
	inputArr := []float64{-25.4, -27.0, 13.0, 19.0, 15.5, 24.5, -21.0, 32.5, 1, -1}
	log.Println(createSubSets(inputArr))
}

//Создание подмножеств на основе кратности 10.
func createSubSets(input []float64) (output map[int][]float64) {
	output = make(map[int][]float64)
	for _, val := range input {
		rounded := int(math.Round(val)/10) * 10
		if slice, ok := output[rounded]; ok {
			output[rounded] = append(slice, val)
		} else {
			output[rounded] = append(make([]float64, 0), val)
		}
	}
	return output
}
